#!/bin/bash

# ========== HOW TO USE ========== 
# Option 1 - Execute the script from the project root directory
# Option 2 - Pass the absolute path to the project as the argument while executing the script

# ========== STRING ==========

intranet_repositories_groovy="<i1>repositories {
<i2>maven { url 'http://artfactop.se.scb.co.th:8081/repository/maven-public' }
<i1>}"
intranet_repositories_kotlin="<i1>repositories {
<i2>maven(\"http://artfactop.se.scb.co.th:8081/repository/maven-public\")
<i1>}"
internet_repositories_groovy="<i1>repositories {
<i2>mavenCentral()
<i2>maven { url 'https://plugins.gradle.org/m2' }
<i2>maven { url 'http://packages.confluent.io/maven' }
<i1>}"
internet_repositories_kotlin="<i1>repositories {
<i2>mavenCentral()
<i2>maven(\"https://plugins.gradle.org/m2\")
<i2>maven(\"http://packages.confluent.io/maven\")
<i1>}"

# ========== FUNCTION ==========

should_repository_toggle_to_internet() {
  project_path=$1
  sampleFile="$project_path/gradle/wrapper/gradle-wrapper.properties"

  if [ -f "$sampleFile" ] ; then
    if grep -q "artfactop" "$sampleFile"; then
      echo "true"
    else 
      echo "false"
    fi
  else 
    exit
  fi
}

replace_repositories_block() {
  project_path=$1
  file_name=$2
  switch_to_internet=$3
  internet_repositories=$4
  intranet_repositories=$5
  file_path="$project_path/$file_name"
  if [ -f "$file_path" ] ; then
    if [ "$switch_to_internet" = true ] && grep -q "artfactop" "$file_path" ; then
      echo "[$file_name] Change to public repositories (mavenCentral)"
      temp=$(cat $file_path | tr '\n' '\\' | sed 's/maven {[^{}]*}//g' | sed 's/repositories {[^{}]*}/<<repositories>>/g' | tr '\\' '\n')
      internet_repositories="${internet_repositories//<i1>/}"
      internet_repositories="${internet_repositories//<i2>/  }"
      echo "${temp//<<repositories>>/$internet_repositories}" > $file_path
    elif [ "$switch_to_internet" = false ] && grep -q "mavenCentral" "$file_path" ; then
      echo "[$file_name] Change to intranet repositories (artfactop.se.scb.co.th)"
      temp=$(cat $file_path | tr '\n' '\\' | sed 's/maven {[^{}]*}//g' | sed 's/repositories {[^{}]*}/<<repositories>>/g' | tr '\\' '\n')
      intranet_repositories="${intranet_repositories//<i1>/}"
      intranet_repositories="${intranet_repositories//<i2>/  }"
      echo "${temp//<<repositories>>/$intranet_repositories}" > $file_path
    else
      echo "[$file_name] No modification needed"
    fi
  fi
}

replace_plugin_management_block() {
  project_path=$1
  file_name=$2
  switch_to_internet=$3
  internet_repositories=$4
  intranet_repositories=$5
  file_path="$project_path/$file_name"
  if [ -f "$file_path" ] ; then
    if [ "$switch_to_internet" = true ] ; then
      echo "[$file_name] Change pluginManagement to public repositories (mavenCentral)"
      temp=$(cat $file_path | tr '\n' '\\' | sed 's/maven {[^{}]*}//g' | sed 's/repositories {[^{}]*}//g' | sed 's/pluginManagement {[^{}]*}\\*//' | tr '\\' '\n')
      echo "$temp" > $file_path
      internet_repositories="${internet_repositories//<i1>/  }"
      internet_repositories="${internet_repositories//<i2>/    }"
      echo "pluginManagement {
$internet_repositories
}
$(cat $file_path)" > $file_path
    elif [ "$switch_to_internet" = false ] ; then
      echo "[$file_name] Change pluginManagement to intranet repositories (artfactop.se.scb.co.th)"
      temp=$(cat $file_path | tr '\n' '\\' | sed 's/maven {[^{}]*}//g' | sed 's/repositories {[^{}]*}//g' | sed 's/pluginManagement {[^{}]*}\\*//' | tr '\\' '\n')
      echo "$temp" > $file_path
      intranet_repositories="${intranet_repositories//<i1>/  }"
      intranet_repositories="${intranet_repositories//<i2>/    }"
      echo "pluginManagement {
$intranet_repositories
}
$(cat $file_path)" > $file_path
    else 
      echo "[$file_name] No modification needed"
    fi
  fi
}

update_project() {
  switch_to_internet=$1
  project_path=$2
  
  echo
  echo "working directory: $project_path"
  echo
  
  gradle_properties="$project_path/gradle/wrapper/gradle-wrapper.properties"
  if [ -f "$gradle_properties" ] ; then
    file_count=$((file_count+1))
    if [ "$switch_to_internet" = true ] && grep -q "artfactop" "$gradle_properties" ; then
      echo "[gradle-wrapper.properties] Change to services.gradle.org"
      $(sed -i "" "s http https g" $gradle_properties)
      $(sed -i "" "s artfactop.se.scb.co.th:8081/repository/gradle_distribution services.gradle.org/distributions g" $gradle_properties)
    elif [ "$switch_to_internet" = false ] && grep -q "services.gradle.org" "$gradle_properties" ; then
      echo "[gradle-wrapper.properties] Change to intranet repositories (artfactop.se.scb.co.th)"
      $(sed -i "" "s https http g" $gradle_properties)
      $(sed -i "" "s services.gradle.org/distributions artfactop.se.scb.co.th:8081/repository/gradle_distribution g" $gradle_properties)
    else 
      echo "[gradle-wrapper.properties] No modification needed"
    fi
  fi

  # For Groovy Gradle files
  replace_repositories_block $project_path "build.gradle" $switch_to_internet "$internet_repositories_groovy" "$intranet_repositories_groovy"
  replace_plugin_management_block $project_path "settings.gradle" $switch_to_internet "$internet_repositories_groovy" "$intranet_repositories_groovy"
  
  # For Kotlin Gradle files
  replace_repositories_block $project_path "build.gradle.kts" $switch_to_internet "$internet_repositories_kotlin" "$intranet_repositories_kotlin"
  replace_plugin_management_block $project_path "settings.gradle.kts" $switch_to_internet "$internet_repositories_kotlin" "$intranet_repositories_kotlin"
  replace_repositories_block $project_path "buildSrc/build.gradle.kts" $switch_to_internet "$internet_repositories_kotlin" "$intranet_repositories_kotlin"
  replace_plugin_management_block $project_path "buildSrc/settings.gradle.kts" $switch_to_internet "$internet_repositories_kotlin" "$intranet_repositories_kotlin"
}

# ========== MAIN PROGRAM ==========

if [ $1 ] ; then 
  input_path=$1
else
  input_path="$PWD"
fi

switch_to_internet=$(should_repository_toggle_to_internet $input_path)
# switch_to_internet="false"
update_project $switch_to_internet "$input_path"
update_project $switch_to_internet "$input_path/../kafka-connector-core"
update_project $switch_to_internet "$input_path/../mq-connector-core"
update_project $switch_to_internet "$input_path/../nss-mq-connector"
update_project $switch_to_internet "$input_path/../springboot-framework"
update_project $switch_to_internet "$input_path/../springboot-message-delivery"
update_project $switch_to_internet "$input_path/../springboot-pubsub"

echo
echo "All operations complete"